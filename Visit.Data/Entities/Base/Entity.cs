﻿namespace Visit.Data.Entities.Base;

public abstract record Entity(int Id) : IEntity
{
    public int Id { get; set; } = Id;
}