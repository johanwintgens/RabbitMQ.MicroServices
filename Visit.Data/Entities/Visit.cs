﻿using System;
using Visit.Data.Entities.Base;

namespace Visit.Data.Entities;

public record Visit(int Id, string VisitorName, string VisitorEmail, int EmployeeId, int CompanyId, DateTime StartTime, DateTime? EndTime = null)
    : Entity(Id);