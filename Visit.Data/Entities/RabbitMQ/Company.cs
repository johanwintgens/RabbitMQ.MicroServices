﻿

using Visit.Data.Entities.Base;

namespace Visit.Data.Entities.RabbitMQ;

public record Company(int Id, string Name) : Entity(Id);