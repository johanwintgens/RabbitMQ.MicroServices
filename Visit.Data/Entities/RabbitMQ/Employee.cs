﻿using Visit.Data.Entities.Base;

namespace Visit.Data.Entities.RabbitMQ;

public record Employee(int Id, string Name, string Email, int CompanyId) : Entity(Id);