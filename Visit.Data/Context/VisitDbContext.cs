﻿using Microsoft.EntityFrameworkCore;

namespace Visit.Data.Context;

class VisitDbContext : DbContext
{
    const bool UseDatabase = false;
    const string DatabaseName = "VisitDb";

    public DbSet<Entities.Visit> Visits { get; set; }

    public VisitDbContext()
    {
        if (!UseDatabase)
            Seed();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (UseDatabase)
            optionsBuilder.UseSqlServer($"server=.;database={DatabaseName};trusted_connection=true;");
        optionsBuilder.UseInMemoryDatabase(DatabaseName);
    }

    void Seed()
    {
        //if (Visits.Any(x => x.Id == 1))
        //    return;

        //var visits = new[]
        //{
        //    new Entities.Visit(1, "John Doe", "john.doe@actemium.be", "Jane Doe", "AllPhi", DateTime.Now, DateTime.Now.AddMinutes(10))
        //};

        //Visits.AddRange(visits);

        //SaveChanges();
    }
}