﻿using Microsoft.EntityFrameworkCore;
using Visit.Data.Entities.RabbitMQ;

namespace Visit.Data.Context;

public class RabbitMQContext : DbContext
{
    const bool UseDatabase = false;
    const string DatabaseName = "VisitDb";

    public DbSet<Company> Companies { get; set; }
    public DbSet<Employee> Employees { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (UseDatabase)
            optionsBuilder.UseSqlServer($"server=.;database={DatabaseName};trusted_connection=true;");
        optionsBuilder.UseInMemoryDatabase(DatabaseName);
    }
}