using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Visit.Data.Context;
using Visit.Data.Repos.Base;

namespace Visit.Data.Extensions;

public static class ServiceCollectionExtensions
{
    public static void ConfigureDataDI(this IServiceCollection services)
    {
        // Data Layer
        services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

        services.AddTransient<DbContext, VisitDbContext>();
    }
}