﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Visit.Data.Repos.Base;

public interface IRepository<TEntity> 
{
    Task<TEntity> GetAsync(int id, CancellationToken cancellationToken = default);
    Task<IQueryable<TEntity>> GetAllAsync(CancellationToken cancellationToken = default);
    Task<int> CreateAsync(TEntity entity, CancellationToken cancellationToken = default);
    Task UpdateAsync(TEntity entity, CancellationToken cancellationToken = default);
    Task DeleteAsync(int id, CancellationToken cancellationToken = default);
    Task<bool> ExistsAsync(int id, CancellationToken cancellationToken);
}