using Microsoft.Extensions.DependencyInjection;
using Visit.Business.Mappers;
using Visit.Business.Mappers.Base;
using Visit.Business.Services;
using Visit.Business.Services.Base;
using Visit.Business.Services.Interfaces;
using Visit.Data.Extensions;
using Visit.RabbitMQ.Publisher;
using VisitDto = Visit.Business.DTOs.Visit;
using VisitEntity = Visit.Data.Entities.Visit;

namespace Visit.Business.Extensions;

public static class ServiceCollectionExtensions
{
    public static void ConfigureBusinessDI(this IServiceCollection services)
    {
        // Business Layer
        services.AddScoped<IVisitService, VisitService>();
        
        services.AddScoped<IService<VisitDto>, VisitService>();
        
        services.AddScoped(typeof(IPublisher<>), typeof(Publisher<>));

        services.AddScoped<IMapper<VisitEntity,VisitDto>, VisitMapper>();
            
        services.ConfigureDataDI();
    }
}