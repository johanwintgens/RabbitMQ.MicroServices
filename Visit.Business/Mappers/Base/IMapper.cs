﻿namespace Visit.Business.Mappers.Base;

public interface IMapper<TEntity, TDto>
{
    TDto Map(TEntity source);
    TEntity Map(TDto source);
    ICollection<TEntity> Map(IEnumerable<TDto> sources);
    ICollection<TDto> Map(IEnumerable<TEntity> sources);
}