﻿using Visit.Business.Mappers.Base;
using VisitDto = Visit.Business.DTOs.Visit;
using VisitEntity = Visit.Data.Entities.Visit;

namespace Visit.Business.Mappers;

class VisitMapper : IMapper<VisitEntity, VisitDto>
{
    public VisitDto Map(VisitEntity source)
    {
        return new VisitDto(
            source.Id,
            source.VisitorName,
            source.VisitorEmail,
            source.EmployeeId,
            source.CompanyId,
            source.StartTime,
            source.EndTime
        );
    }

    public VisitEntity Map(VisitDto source)
    {
        return new VisitEntity(
            source.Id,
            source.VisitorName,
            source.VisitorEmail,
            source.EmployeeId,
            source.CompanyId,
            source.StartTime,
            source.EndTime
        );
    }

    public ICollection<VisitDto> Map(IEnumerable<VisitEntity> sources) => sources.Select(Map).ToList();
    public ICollection<VisitEntity> Map(IEnumerable<VisitDto> sources) => sources.Select(Map).ToList();
}