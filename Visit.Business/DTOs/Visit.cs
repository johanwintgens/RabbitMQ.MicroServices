﻿using Visit.Business.DTOs.Base;

namespace Visit.Business.DTOs;

public record Visit(int Id, string VisitorName, string VisitorEmail, int EmployeeId, int CompanyId, DateTime StartTime, DateTime? EndTime = null)
    : Dto(Id)
{
    public DateTime StartTime { get; set; } = StartTime;
    public DateTime? EndTime { get; set; } = EndTime;
}