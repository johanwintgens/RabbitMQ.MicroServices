﻿using Visit.Business.Mappers.Base;
using Visit.Data.Entities.Base;
using Visit.Data.Repos.Base;
using Visit.RabbitMQ.Publisher;

namespace Visit.Business.Services.Base;

public abstract class Service<TEntity, TDto> : IService<TDto> where TEntity : Entity
{
    protected readonly IRepository<TEntity> Repo;
    protected readonly IMapper<TEntity, TDto> Mapper;
    protected readonly IPublisher<TEntity> Publisher;

    protected Service(IRepository<TEntity> repo, IMapper<TEntity, TDto> mapper, IPublisher<TEntity> publisher)
    {
        Repo = repo;
        Mapper = mapper;
        Publisher = publisher;
    }

    public async Task<TDto> GetAsync(int id, CancellationToken cancellationToken = default)
    {
        var entity = await Repo.GetAsync(id, cancellationToken);

        return Mapper.Map(entity);
    }

    public async Task<ICollection<TDto>> GetAllAsync(CancellationToken cancellationToken = default)
    {
        var entities = await Repo.GetAllAsync(cancellationToken);

        return Mapper.Map(entities);
    }

    public async Task<int> CreateAsync(TDto dto, CancellationToken cancellationToken = default)
    {
        var entity = Mapper.Map(dto);

        entity.Id = await Repo.CreateAsync(entity, cancellationToken);

        Publisher.Publish(CrudAction.Create, entity);

        return entity.Id;
    }

    public async Task UpdateAsync(TDto dto, CancellationToken cancellationToken = default)
    {
        var entity = Mapper.Map(dto);

        await Repo.UpdateAsync(entity, cancellationToken);

        Publisher.Publish(CrudAction.Update, entity);
    }

    public async Task DeleteAsync(int id, CancellationToken cancellationToken = default)
    {
        await Repo.DeleteAsync(id, cancellationToken);

        Publisher.Publish(CrudAction.Delete, id);
    }

    public async Task<bool> ExistsAsync(int id, CancellationToken cancellationToken)
    {
        return await Repo.ExistsAsync(id, cancellationToken);
    }
}