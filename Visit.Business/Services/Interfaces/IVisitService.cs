﻿using Visit.Business.Services.Base;

namespace Visit.Business.Services.Interfaces;

public interface IVisitService : IService<DTOs.Visit>
{
}