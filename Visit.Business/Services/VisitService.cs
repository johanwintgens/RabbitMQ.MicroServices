﻿using Visit.Business.Mappers.Base;
using Visit.Business.Services.Base;
using Visit.Business.Services.Interfaces;
using Visit.Data.Repos.Base;
using Visit.RabbitMQ.Publisher;
using VisitDto = Visit.Business.DTOs.Visit;
using VisitEntity = Visit.Data.Entities.Visit;

namespace Visit.Business.Services;

public class VisitService : Service<VisitEntity, VisitDto>, IVisitService
{
    public VisitService(IRepository<VisitEntity> repo, IMapper<VisitEntity, VisitDto> mapper, IPublisher<VisitEntity> publisher) 
        : base(repo, mapper, publisher)
    {
    }
}