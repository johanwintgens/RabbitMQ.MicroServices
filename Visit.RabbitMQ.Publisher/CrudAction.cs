﻿namespace Visit.RabbitMQ.Publisher;

public enum CrudAction
{
    Create,
    Update,
    Delete
}