﻿using System.Text;
using Company.Data.Context;
using Company.Data.Entities.RabbitMQ;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Company.RabbitMQ.Subscriber;

public class Subscriber
{
    static readonly string Queue = "VisitQueue";
    static readonly string Exchange = "VisitExchange";

    static IModel? _channel;
    static IConnection? _connection;
    static EventingBasicConsumer? _consumer;
    static DbContext? _context;

    static readonly ManualResetEvent QuitEvent = new(false);

    public static void Main()
    {
        Initialize();

        _consumer.Received += OnReceived;

        _channel.BasicConsume(
            queue: Queue,
            autoAck: true,
            consumer: _consumer);

        QuitEvent.WaitOne();
    }

    static void Initialize()
    {
        var serviceProvider = new ServiceCollection()
                              .AddScoped<DbContext, RabbitMQContext>()
                              .BuildServiceProvider();

        _context = serviceProvider.GetRequiredService<DbContext>();

        var factory = new ConnectionFactory { HostName = "localhost" };

        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();

        _channel.ExchangeDeclare(Exchange, ExchangeType.Fanout);
        _channel.QueueDeclare(Queue);
        _channel.QueueBind(queue: Queue, exchange: Exchange, routingKey: "");

        _consumer = new EventingBasicConsumer(_channel);
    }

    static void OnReceived(object? model, BasicDeliverEventArgs ea)
    {
        var body = ea.Body.ToArray();
        var message = Encoding.UTF8.GetString(body);
        var type = ea.RoutingKey;

        switch (type)
        {
            default:
                throw new ArgumentException();

            case "Visit.Create":
            {
                var entity = JsonConvert.DeserializeObject<Visit>(message);

                _context.Set<Visit>().Add(entity);

                break;
            }

            case "Visit.Update":
            {
                var entity = JsonConvert.DeserializeObject<Visit>(message);

                var original = _context.Set<Visit>().First(x => x.Id == entity.Id);

                _context.Entry(original)
                        .CurrentValues
                        .SetValues(entity);

                break;
            }

            case "Visit.Delete":
            {
                var id = Convert.ToInt32(message);

                var entity = _context.Set<Visit>().First(x => x.Id == id);

                _context.Set<Visit>().Remove(entity);

                break;
            }
        }

        _context.SaveChanges();
    }
}