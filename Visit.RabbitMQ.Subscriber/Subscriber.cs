﻿using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Visit.Data.Context;
using Visit.Data.Entities.RabbitMQ;

namespace Visit.RabbitMQ.Subscriber;

public class Subscriber
{
    static readonly string Queue = "CompanyQueue";
    static readonly string Exchange = "CompanyExchange";

    static IModel? _channel;
    static IConnection? _connection;
    static EventingBasicConsumer? _consumer;
    static DbContext? _context;

    static readonly ManualResetEvent QuitEvent = new(false);

    public static void Main()
    {
        Initialize();

        _consumer.Received += OnReceived;

        _channel.BasicConsume(
            queue: Queue,
            autoAck: true,
            consumer: _consumer);

        QuitEvent.WaitOne();
    }

    static void Initialize()
    {
        var serviceProvider = new ServiceCollection()
                              .AddScoped<DbContext, RabbitMQContext>()
                              .BuildServiceProvider();

        _context = serviceProvider.GetRequiredService<DbContext>();

        var factory = new ConnectionFactory { HostName = "localhost" };

        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();

        _channel.ExchangeDeclare(Exchange, ExchangeType.Fanout);
        _channel.QueueDeclare(Queue);
        _channel.QueueBind(queue: Queue, exchange: Exchange, routingKey: "");

        _consumer = new EventingBasicConsumer(_channel);
    }

    static void OnReceived(object? model, BasicDeliverEventArgs ea)
    {
        var body = ea.Body.ToArray();
        var message = Encoding.UTF8.GetString(body);
        var type = ea.RoutingKey;

        switch (type)
        {
            default:
                throw new ArgumentException();

            case "Company.Create":
            {
                var entity = JsonConvert.DeserializeObject<Company>(message);

                _context.Set<Company>().Add(entity);

                break;
            }

            case "Company.Update":
            {
                var entity = JsonConvert.DeserializeObject<Company>(message);

                var original = _context.Set<Company>().First(x => x.Id == entity.Id);

                _context.Entry(original)
                        .CurrentValues
                        .SetValues(entity);

                break;
            }

            case "Company.Delete":
            {
                var id = Convert.ToInt32(message);

                var entity = _context.Set<Company>().First(x => x.Id == id);

                _context.Set<Company>().Remove(entity);

                break;
            }

            case "Employee.Create":
            {
                var entity = JsonConvert.DeserializeObject<Employee>(message);

                _context.Set<Employee>().Add(entity);

                break;
            }

            case "Employee.Update":
            {
                var entity = JsonConvert.DeserializeObject<Employee>(message);

                var original = _context.Set<Employee>().First(x => x.Id == entity.Id);

                _context.Entry(original)
                        .CurrentValues
                        .SetValues(entity);

                break;
            }

            case "Employee.Delete":
            {
                var id = Convert.ToInt32(message);

                var entity = _context.Set<Employee>().First(x => x.Id == id);

                _context.Set<Employee>().Remove(entity);

                break;
            }
        }

        _context.SaveChanges();
    }
}