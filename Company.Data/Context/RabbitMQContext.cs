﻿using Company.Data.Entities.RabbitMQ;
using Microsoft.EntityFrameworkCore;

namespace Company.Data.Context;

public class RabbitMQContext : DbContext
{
    const bool UseDatabase = false;
    const string DatabaseName = "CompanyDb";

    public DbSet<Visit> Visits { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (UseDatabase)
            optionsBuilder.UseSqlServer($"server=.;database={DatabaseName};trusted_connection=true;");
        optionsBuilder.UseInMemoryDatabase(DatabaseName);
    }
}