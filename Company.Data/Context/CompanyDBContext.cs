﻿using Company.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Company.Data.Context;

class CompanyDbContext : DbContext
{
    const bool UseDatabase = false;
    const string DatabaseName = "CompanyDb";

    public DbSet<Entities.Company> Companies { get; set; }
    public DbSet<Employee> Employees { get; set; }

    public CompanyDbContext()
    {
        if (!UseDatabase)
            Seed();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (UseDatabase)
            optionsBuilder.UseSqlServer($"server=.;database={DatabaseName};trusted_connection=true;");
        optionsBuilder.UseInMemoryDatabase(DatabaseName);
    }

    void Seed()
    {
        //if (Companies.Any(x => x.Id == 1))
        //    return;

        //if (Employees.Any(x => x.Id == 1))
        //    return;

        //var companies = new[]
        //{
        //    new Entities.Company(1, "AllPhi"),
        //    new Entities.Company(2, "Actemium")
        //};

        //var employees = new[]
        //{
        //    new Employee(1, "Johan Wintgens", "johan.wintgens@allphi.eu", 1),
        //    new Employee(2, "John Doe", "john.doe@actemium.be", 2)
        //};

        //Companies.AddRange(companies);
        //Employees.AddRange(employees);

        //SaveChanges();
    }
}