using Company.Data.Context;
using Company.Data.Entities;
using Company.Data.Repos;
using Company.Data.Repos.Base;
using Company.Data.Repos.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Company.Data.Extensions;

public static class ServiceCollectionExtensions
{
    public static void ConfigureDataDI(this IServiceCollection services)
    {
        // Data Layer
        services.AddScoped<IRepository<Employee>, EmployeeRepository>();
        services.AddScoped<IEmployeeRepository, EmployeeRepository>();

        services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

        services.AddTransient<DbContext, CompanyDbContext>();
    }
}