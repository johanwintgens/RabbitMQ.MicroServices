﻿using Company.Data.Entities.Base;

namespace Company.Data.Entities;

public record Employee(int Id, string Name, string Email, int CompanyId) : Entity(Id);