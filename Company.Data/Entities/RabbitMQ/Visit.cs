﻿using System;
using Company.Data.Entities.Base;

namespace Company.Data.Entities.RabbitMQ;

public record Visit(int Id, string VisitorName, string VisitorEmail, int EmployeeId, int CompanyId, DateTime StartTime, DateTime? EndTime = null)
    : Entity(Id);