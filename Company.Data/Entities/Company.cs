﻿using Company.Data.Entities.Base;

namespace Company.Data.Entities;

public record Company(int Id, string Name) : Entity(Id);