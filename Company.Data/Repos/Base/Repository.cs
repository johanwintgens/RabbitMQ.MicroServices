﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Company.Data.Entities.Base;
using Microsoft.EntityFrameworkCore;

namespace Company.Data.Repos.Base;

public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
{
    protected readonly DbContext Context;
    protected DbSet<TEntity> DbSet { get; }

    public Repository(DbContext context)
    {
        Context = context;
        DbSet = Context.Set<TEntity>();
    }

    public Task<TEntity> GetAsync(int id, CancellationToken cancellationToken = default)
    {
        return DbSet.FirstOrDefaultAsync(e => e.Id == id, cancellationToken);
    }

    public Task<IQueryable<TEntity>> GetAllAsync(CancellationToken cancellationToken = default)
    {
        var source = new TaskCompletionSource<IQueryable<TEntity>>();

        try
        {
            cancellationToken.ThrowIfCancellationRequested();

            source.SetResult(DbSet.AsNoTracking());
        }
        catch (OperationCanceledException)
        {
            source.SetCanceled(cancellationToken);
        }

        return source.Task;
    }

    public async Task<int> CreateAsync(TEntity entity, CancellationToken cancellationToken = default)
    {
        await DbSet.AddAsync(entity, cancellationToken);
        await Context.SaveChangesAsync(cancellationToken);

        return entity.Id;
    }

    public async Task UpdateAsync(TEntity entity, CancellationToken cancellationToken = default)
    {
        var e = await DbSet.FirstOrDefaultAsync(e => e.Id == entity.Id, cancellationToken);

        Context.Entry(e)
               .CurrentValues
               .SetValues(entity);

        await Context.SaveChangesAsync(cancellationToken);
    }

    public async Task DeleteAsync(int id, CancellationToken cancellationToken = default)
    {
        var e = await DbSet.FirstOrDefaultAsync(e => e.Id == id, cancellationToken);

        DbSet.Remove(e);

        await Context.SaveChangesAsync(cancellationToken);
    }

    public Task<bool> ExistsAsync(int id, CancellationToken cancellationToken)
    {
        return DbSet.AnyAsync(x => x.Id == id, cancellationToken);
    }
}