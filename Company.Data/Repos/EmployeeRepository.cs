﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Company.Data.Entities;
using Company.Data.Repos.Base;
using Company.Data.Repos.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Company.Data.Repos;

public class EmployeeRepository : Repository<Employee>, IEmployeeRepository
{
    public EmployeeRepository(DbContext context) : base(context)
    {
    }

    public Task<IQueryable<Employee>> GetAllFromCompany(int companyId, CancellationToken cancellationToken = default)
    {
        return Task.FromResult(DbSet.Where(e => e.CompanyId == companyId));
    }
}