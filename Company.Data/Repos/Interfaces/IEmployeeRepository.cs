﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Company.Data.Entities;
using Company.Data.Repos.Base;

namespace Company.Data.Repos.Interfaces;

public interface IEmployeeRepository : IRepository<Employee>
{
    public Task<IQueryable<Employee>> GetAllFromCompany(int companyId, CancellationToken cancellationToken = default);
}