﻿namespace Company.Application.Exceptions;

class InvalidPayloadException : Exception
{
    public InvalidPayloadException(string type)
        : base($"Cannot convert Payload to {type}")
    {
    }
}