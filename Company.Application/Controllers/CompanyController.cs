﻿using Company.Application.Handlers.Base;
using Company.Application.Requests;
using Company.Application.Responses.Base;
using Microsoft.AspNetCore.Mvc;
using CompanyDto = Company.Business.DTOs.Company;

namespace Company.Application.Controllers;

[ApiController]
[Route("[controller]")]
public class CompanyController : Controller
{
    readonly IHandler<CompanyDto> _handler;

    public CompanyController(IHandler<CompanyDto> handler)
    {
        _handler = handler;
    }

    [HttpGet]
    public async Task<IActionResult> GetAll(CancellationToken cancellationToken)
    {
        try
        {
            IResponse response = await _handler.GetAllAsync(cancellationToken);

            return Ok(response);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id, CancellationToken cancellationToken)
    {
        try
        {
            IResponse response = await _handler.GetAsync(id, cancellationToken);

            return Ok(response);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromBody] CreateCompanyRequest createCompanyRequest, CancellationToken cancellationToken)
    {
        try
        {
            IResponse response = await _handler.CreateAsync(createCompanyRequest, cancellationToken);

            return Ok(response);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPut("{id:int}")]
    public async Task<IActionResult> Update(int id, [FromBody] UpdateCompanyRequest updateCompanyRequest, CancellationToken cancellationToken)
    {
        try
        {
            await _handler.UpdateAsync(id, updateCompanyRequest, cancellationToken);

            return Ok();
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpDelete("{id:int}")]
    public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
    {
        try
        {
            await _handler.DeleteAsync(id, cancellationToken);

            return Ok();
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }
}