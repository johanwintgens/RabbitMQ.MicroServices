﻿using Company.Application.Handlers.Interfaces;
using Company.Application.Requests;
using Company.Application.Responses.Base;
using Microsoft.AspNetCore.Mvc;

namespace Company.Application.Controllers;

[ApiController]
[Route("[controller]")]
public class EmployeeController : Controller
{
    readonly IEmployeeHandler _handler;

    public EmployeeController(IEmployeeHandler handler)
    {
        _handler = handler;
    }
    
    [HttpGet("Company/{companyId}")]
    public async Task<IActionResult> GetCompanyEmployees(int companyId, CancellationToken cancellationToken = default)
    {
        try
        {
            IResponse response = await _handler.GetAllFromCompanyAsync(companyId, cancellationToken);

            return Ok(response);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpGet]
    public async Task<IActionResult> GetAll(CancellationToken cancellationToken = default)
    {
        try
        {
            IResponse response = await _handler.GetAllAsync(cancellationToken);

            return Ok(response);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id, CancellationToken cancellationToken)
    {
        try
        {
            IResponse response = await _handler.GetAsync(id, cancellationToken);

            return Ok(response);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromBody] CreateEmployeeRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            IResponse response = await _handler.CreateAsync(request, cancellationToken);

            return Ok(response);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPut("{id:int}")]
    public async Task<IActionResult> Update(int id, [FromBody] UpdateEmployeeRequest updateEmployeeRequest, CancellationToken cancellationToken)
    {
        try
        {
            await _handler.UpdateAsync(id, updateEmployeeRequest, cancellationToken);

            return Ok();
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpDelete("{id:int}")]
    public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
    {
        try
        {
            await _handler.DeleteAsync(id, cancellationToken);

            return Ok();
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }
}