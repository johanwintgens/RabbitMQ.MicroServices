﻿using Company.Application.Requests.Base;

namespace Company.Application.Requests;

public record CreateCompanyRequest(string Name) : IRequest;