﻿using Company.Application.Requests.Base;

namespace Company.Application.Requests;

public record CreateEmployeeRequest(string Name, string Email, int CompanyId) : IRequest;