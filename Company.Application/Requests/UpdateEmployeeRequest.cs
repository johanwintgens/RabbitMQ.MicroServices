﻿using Company.Application.Requests.Base;

namespace Company.Application.Requests;

public record UpdateEmployeeRequest(string Name, string Email, int CompanyId) : IRequest;