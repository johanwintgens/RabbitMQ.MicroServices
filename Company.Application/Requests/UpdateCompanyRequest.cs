﻿using Company.Application.Requests.Base;

namespace Company.Application.Requests;

public record UpdateCompanyRequest(string Name) : IRequest;