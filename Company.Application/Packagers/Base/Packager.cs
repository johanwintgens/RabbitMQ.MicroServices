﻿using Company.Application.Exceptions;
using Company.Application.Requests.Base;
using Company.Application.Responses.Base;
using Company.Business.DTOs.Base;
using Newtonsoft.Json;

namespace Company.Application.Packagers.Base;

class Packager : IPackager
{
    public TDto Extract<TDto>(IRequest request) where TDto : class
    {
        var json = JsonConvert.SerializeObject(request);
        var dto = JsonConvert.DeserializeObject<TDto>(json);

        if (dto == null)
            throw new InvalidPayloadException(typeof(TDto).Name);

        return dto;
    }

    public IResponse CreateResponse(object dto)
    {
        return new Response(dto);
    }
}