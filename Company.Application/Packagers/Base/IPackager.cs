﻿using Company.Application.Requests.Base;
using Company.Application.Responses.Base;

namespace Company.Application.Packagers.Base;

public interface IPackager
{
    TDto Extract<TDto>(IRequest request) where TDto : class;
    IResponse CreateResponse(object dto);
}