﻿using Company.Application.Handlers.Base;
using Company.Application.Responses.Base;
using EmployeeDto = Company.Business.DTOs.Employee;

namespace Company.Application.Handlers.Interfaces;

public interface IEmployeeHandler : IHandler<EmployeeDto>
{
    Task<IResponse> GetAllFromCompanyAsync(int companyId, CancellationToken cancellationToken = default);
}