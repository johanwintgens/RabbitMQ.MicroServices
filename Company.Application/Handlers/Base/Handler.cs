﻿using Company.Application.Exceptions;
using Company.Application.Packagers.Base;
using Company.Application.Requests.Base;
using Company.Application.Responses.Base;
using Company.Business.DTOs.Base;
using Company.Business.Services.Base;

namespace Company.Application.Handlers.Base;

public class Handler<TDto> : IHandler<TDto> where TDto : Dto
{
    protected readonly IPackager Packager;
    protected readonly IService<TDto> Service;

    public Handler(IPackager packager, IService<TDto> service)
    {
        Packager = packager;
        Service = service;
    }

    public async Task<IResponse> GetAsync(int id, CancellationToken cancellationToken = default)
    {
        await ExistenceCheck(id, cancellationToken);

        var dto = await Service.GetAsync(id, cancellationToken);

        return Packager.CreateResponse(dto);
    }

    public async Task<IResponse> GetAllAsync(CancellationToken cancellationToken = default)
    {
        var dtos = await Service.GetAllAsync(cancellationToken);

        return Packager.CreateResponse(dtos);
    }

    public async Task<IResponse> CreateAsync(IRequest request, CancellationToken cancellationToken = default)
    {
        var dto = Packager.Extract<TDto>(request);

        await NonExistenceCheck(dto.Id, cancellationToken);

        var id = await Service.CreateAsync(dto, cancellationToken);

        return Packager.CreateResponse(id);
    }

    public async Task UpdateAsync(int id, IRequest request, CancellationToken cancellationToken = default)
    {
        var dto = Packager.Extract<TDto>(request);

        dto.Id = id;

        await ExistenceCheck(dto.Id, cancellationToken);

        await Service.UpdateAsync(dto, cancellationToken);
    }

    public async Task DeleteAsync(int id, CancellationToken cancellationToken = default)
    {
        await ExistenceCheck(id, cancellationToken);

        await Service.DeleteAsync(id, cancellationToken);
    }


    #region Exception Handling

    async Task ExistenceCheck(int id, CancellationToken cancellationToken)
    {
        var exists = await Service.ExistsAsync(id, cancellationToken);
        if (!exists)
            throw new EntityNotFoundException<TDto>(id);
    }

    async Task NonExistenceCheck(int id, CancellationToken cancellationToken)
    {
        var exists = await Service.ExistsAsync(id, cancellationToken);
        if (exists)
            throw new EntityExistsException<TDto>(id);
    }

    #endregion
}