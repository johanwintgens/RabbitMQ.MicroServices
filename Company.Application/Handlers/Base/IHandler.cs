﻿using Company.Application.Requests.Base;
using Company.Application.Responses.Base;

namespace Company.Application.Handlers.Base;

public interface IHandler<TDto>
{
    Task<IResponse> GetAsync(int id, CancellationToken cancellationToken = default);
    Task<IResponse> GetAllAsync(CancellationToken cancellationToken = default);
    Task<IResponse> CreateAsync(IRequest request, CancellationToken cancellationToken = default);
    Task UpdateAsync(int id, IRequest request, CancellationToken cancellationToken = default);
    Task DeleteAsync(int id, CancellationToken cancellationToken = default);
}