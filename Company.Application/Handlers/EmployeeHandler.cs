﻿using Company.Application.Handlers.Base;
using Company.Application.Handlers.Interfaces;
using Company.Application.Packagers.Base;
using Company.Application.Responses.Base;
using Company.Business.Services.Interfaces;
using EmployeeDto = Company.Business.DTOs.Employee;

namespace Company.Application.Handlers;

public class EmployeeHandler : Handler<EmployeeDto>, IEmployeeHandler
{
    protected new readonly IEmployeeService Service;

    public EmployeeHandler(IPackager packager, IEmployeeService service) : base(packager, service)
    {
        Service = service;
    }

    public async Task<IResponse> GetAllFromCompanyAsync(int companyId, CancellationToken cancellationToken = default)
    {
        var dtos = await Service.GetAllFromCompany(companyId, cancellationToken);

        return Packager.CreateResponse(dtos);
    }
}