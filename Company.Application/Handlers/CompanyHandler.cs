﻿using Company.Application.Handlers.Base;
using Company.Application.Packagers.Base;
using Company.Business.Services.Interfaces;
using CompanyDto = Company.Business.DTOs.Company;
using CompanyEntity = Company.Data.Entities.Company;

namespace Company.Application.Handlers;

public class CompanyHandler : Handler<CompanyDto>
{
    protected new readonly ICompanyService Service;

    public CompanyHandler(IPackager packager, ICompanyService service) : base(packager, service)
    {
        Service = service;
    }
}