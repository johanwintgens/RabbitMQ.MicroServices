using Company.Application.Handlers;
using Company.Application.Handlers.Base;
using Company.Application.Handlers.Interfaces;
using Company.Application.Packagers.Base;
using Company.Business.DTOs;
using Company.Business.Extensions;

namespace Company.Application.Extensions;

public static class ServiceCollectionExtensions
{
    public static void ConfigureApplicationDI(this IServiceCollection services)
    {
        // Application Layer
        services.AddScoped<IHandler<Employee>, EmployeeHandler>();
        services.AddScoped<IEmployeeHandler, EmployeeHandler>();
        services.AddScoped<IPackager, Packager>();

        services.AddScoped(typeof(IHandler<>), typeof(Handler<>));

        services.ConfigureBusinessDI();
    }
}