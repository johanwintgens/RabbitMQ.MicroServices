﻿namespace Company.Application.Responses.Base;

class Response : IResponse
{
    public Response(object payload)
    {
        Payload = payload;
    }

    public object Payload { get; set; }
}