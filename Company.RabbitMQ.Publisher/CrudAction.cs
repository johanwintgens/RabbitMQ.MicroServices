﻿namespace Company.RabbitMQ.Publisher;

public enum CrudAction
{
    Create,
    Update,
    Delete
}