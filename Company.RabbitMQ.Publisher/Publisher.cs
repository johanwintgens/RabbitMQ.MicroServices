﻿using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Company.RabbitMQ.Publisher;

public interface IPublisher<TEntity>
{
    void Publish(CrudAction action, object payload);
}

public class Publisher<TEntity> : IPublisher<TEntity>
{
    IModel? _channel;
    IConnection? _connection;

    readonly string _entity;
    readonly string _exchange = "CompanyExchange"; // TODO: Separate Employee & Company Exchange?

    public Publisher()
    {
        _entity = typeof(TEntity).Name;
        //_exchange = $"{_entity}Exchange";

        Init();
    }

    void Init()
    {
        var factory = new ConnectionFactory { HostName = "localhost" };

        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();

        _channel.ExchangeDeclare(_exchange, ExchangeType.Fanout);
    }

    public void Publish(CrudAction action, object payload)
    {
        var json = JsonConvert.SerializeObject(payload);
        var body = Encoding.UTF8.GetBytes(json);

        _channel.BasicPublish(
            exchange: _exchange,
            routingKey: $"{_entity}.{action}",
            basicProperties: null,
            body: body);
    }
}