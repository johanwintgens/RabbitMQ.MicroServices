using Microsoft.AspNetCore.Mvc;
using Visit.Application.Handlers;
using Visit.Application.Requests;
using Visit.Application.Responses.Base;

namespace Visit.Application.Controllers;

[ApiController]
[Route("[controller]")]
public class VisitController : ControllerBase
{
    readonly IVisitHandler _handler;

    public VisitController(IVisitHandler handler)
    {
        _handler = handler;
    }

    [HttpGet]
    public async Task<IActionResult> GetAll(CancellationToken cancellationToken = default)
    {
        try
        {
            IResponse response = await _handler.GetAllAsync(cancellationToken);

            return Ok(response);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id, CancellationToken cancellationToken)
    {
        try
        {
            IResponse response = await _handler.GetAsync(id, cancellationToken);

            return Ok(response);
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromBody] CreateVisitRequest request, CancellationToken cancellationToken = default)
    {
        IResponse response = await _handler.CreateAsync(request, cancellationToken);

        return Ok(response);
    }

    [HttpPut("{id:int}")]
    public async Task<IActionResult> Update(int id, [FromBody] UpdateVisitRequest updateVisitRequest, CancellationToken cancellationToken)
    {
        try
        {
            await _handler.UpdateAsync(id, updateVisitRequest, cancellationToken);

            return Ok();
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpDelete("{id:int}")]
    public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
    {
        try
        {
            await _handler.DeleteAsync(id, cancellationToken);

            return Ok();
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }

    [HttpPut("close/{id:int}")]
    public async Task<IActionResult> Close(int id, CancellationToken cancellationToken)
    {
        try
        {
            await _handler.CloseAsync(id, cancellationToken);

            return Ok();
        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }
}