﻿namespace Visit.Application.Exceptions;

public class EntityExistsException<TDto> : Exception
{
    public EntityExistsException(int id)
        : base($"{typeof(TDto).Name} with ID {id} already exists")
    {
    }
}