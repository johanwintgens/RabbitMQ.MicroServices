﻿namespace Visit.Application.Exceptions;

public class EntityNotFoundException<TDto> : Exception
{
    public EntityNotFoundException(int id)
        : base($"Could not find {typeof(TDto).Name} with ID {id}")
    {
    }
}