﻿using Visit.Application.Requests.Base;
using Visit.Application.Responses.Base;

namespace Visit.Application.Handlers.Base;

public interface IHandler<TDto>
{
    public Task<IResponse> GetAsync(int id, CancellationToken cancellationToken = default);
    public Task<IResponse> GetAllAsync(CancellationToken cancellationToken = default);
    Task<IResponse> CreateAsync(IRequest request, CancellationToken cancellationToken = default);
    public Task UpdateAsync(int id, IRequest request, CancellationToken cancellationToken = default);
    public Task DeleteAsync(int id, CancellationToken cancellationToken = default);
}