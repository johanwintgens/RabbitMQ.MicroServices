﻿using Visit.Application.Handlers.Base;
using Visit.Application.Packagers.Base;
using Visit.Application.Requests.Base;
using Visit.Application.Responses.Base;
using Visit.Business.Services.Base;
using VisitDto = Visit.Business.DTOs.Visit;

namespace Visit.Application.Handlers;

public interface IVisitHandler
{
    Task<IResponse> CreateAsync(IRequest request, CancellationToken cancellationToken = default);
    Task CloseAsync(int id, CancellationToken cancellationToken = default);
    Task<IResponse> GetAsync(int id, CancellationToken cancellationToken = default);
    Task<IResponse> GetAllAsync(CancellationToken cancellationToken = default);
    Task UpdateAsync(int id, IRequest request, CancellationToken cancellationToken = default);
    Task DeleteAsync(int id, CancellationToken cancellationToken = default);
}

public class VisitHandler : Handler<VisitDto>, IVisitHandler
{
    public VisitHandler(IPackager packager, IService<VisitDto> service)
        : base(packager, service)
    {
    }

    public new async Task<IResponse> CreateAsync(IRequest request, CancellationToken cancellationToken = default)
    {
        var dto = Packager.Extract<VisitDto>(request);

        dto.StartTime = DateTime.Now;

        var id = await Service.CreateAsync(dto, cancellationToken);

        return Packager.CreateResponse(id);
    }

    public async Task CloseAsync(int id, CancellationToken cancellationToken = default)
    {
        var dto = await Service.GetAsync(id, cancellationToken);

        dto.EndTime = DateTime.Now;

        await Service.UpdateAsync(dto, cancellationToken);
    }
}