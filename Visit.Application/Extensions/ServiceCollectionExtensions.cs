using Visit.Application.Handlers;
using Visit.Application.Packagers.Base;
using Visit.Business.Extensions;

namespace Visit.Application.Extensions;

public static class ServiceCollectionExtensions
{
    public static void ConfigureApplicationDI(this IServiceCollection services)
    {
        // Application Layer
        services.AddScoped<IVisitHandler, VisitHandler>();

        services.AddScoped<IPackager, Packager>();

        services.ConfigureBusinessDI();
    }
}