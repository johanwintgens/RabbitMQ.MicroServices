﻿namespace Visit.Application.Responses.Base;

public class Response : IResponse
{
    public Response(object? payload)
    {
        Payload = payload;
    }

    public object? Payload { get; set; }
}