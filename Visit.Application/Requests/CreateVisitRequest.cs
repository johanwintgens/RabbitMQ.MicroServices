using Visit.Application.Requests.Base;

namespace Visit.Application.Requests;

public record CreateVisitRequest(string VisitorName, string VisitorEmail, int EmployeeId, int CompanyId) : IRequest
{
    public DateTime StartTime { get; } = DateTime.Now;
}