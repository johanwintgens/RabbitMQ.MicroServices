﻿using Visit.Application.Requests.Base;

namespace Visit.Application.Requests;

public record UpdateVisitRequest(string VisitorName, string VisitorEmail, int EmployeeId, int CompanyId, DateTime StartTime, DateTime? EndTime = null) : IRequest;