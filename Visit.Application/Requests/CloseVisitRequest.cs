﻿using Visit.Application.Requests.Base;

namespace Visit.Application.Requests;

public record CloseVisitRequest(DateTime EndTime) : IRequest;