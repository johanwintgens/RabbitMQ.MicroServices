﻿using Visit.Application.Requests.Base;
using Visit.Application.Responses.Base;

namespace Visit.Application.Packagers.Base;

public interface IPackager
{
    TDto Extract<TDto>(IRequest request);
    IResponse CreateResponse(object? dto);
}