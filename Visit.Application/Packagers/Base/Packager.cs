﻿using Newtonsoft.Json;
using Visit.Application.Requests.Base;
using Visit.Application.Responses.Base;

namespace Visit.Application.Packagers.Base;

public class Packager: IPackager
{
    public TDto Extract<TDto>(IRequest request)
    {
        var json = JsonConvert.SerializeObject(request);
        var dto = JsonConvert.DeserializeObject<TDto>(json);

        if (dto == null)
            throw new InvalidPayloadException(typeof(TDto).Name);

        return dto;
    }

    public IResponse CreateResponse(object? dto)
    {
        return new Response(dto);
    }
}

public class InvalidPayloadException : Exception
{
    public InvalidPayloadException(string type)
        : base($"Cannot convert Payload to {type}")
    {
    }
}