﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Company.Business.Services.Base;

public interface IService<TDto>
{
    Task<TDto> GetAsync(int id, CancellationToken cancellationToken = default);
    Task<ICollection<TDto>> GetAllAsync(CancellationToken cancellationToken = default);
    Task<int> CreateAsync(TDto dto, CancellationToken cancellationToken = default);
    Task UpdateAsync(TDto dto, CancellationToken cancellationToken = default);
    Task DeleteAsync(int id, CancellationToken cancellationToken = default);
    Task<bool> ExistsAsync(int id, CancellationToken cancellationToken);
}