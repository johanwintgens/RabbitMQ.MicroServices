﻿using Company.Business.Services.Base;

namespace Company.Business.Services.Interfaces;

public interface ICompanyService : IService<DTOs.Company>
{
}