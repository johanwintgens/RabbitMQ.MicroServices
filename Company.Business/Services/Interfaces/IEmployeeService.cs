﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Company.Business.DTOs;
using Company.Business.Services.Base;

namespace Company.Business.Services.Interfaces;

public interface IEmployeeService : IService<Employee>
{
    Task<ICollection<Employee>> GetAllFromCompany(int companyId, CancellationToken cancellationToken = default);
}