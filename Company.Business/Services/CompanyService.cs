﻿using Company.Business.Mappers.Base;
using Company.Business.Services.Base;
using Company.Business.Services.Interfaces;
using Company.Data.Repos.Base;
using Company.RabbitMQ.Publisher;
using CompanyDto = Company.Business.DTOs.Company;
using CompanyEntity = Company.Data.Entities.Company;

namespace Company.Business.Services;

public class CompanyService : Service<CompanyEntity, CompanyDto>, ICompanyService
{
    public CompanyService(IRepository<CompanyEntity> repo, IMapper<CompanyEntity, CompanyDto> mapper, IPublisher<CompanyEntity> publisher) 
        : base(repo, mapper, publisher)
    {
    }
}