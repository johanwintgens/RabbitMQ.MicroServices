﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Company.Business.Mappers.Base;
using Company.Business.Services.Base;
using Company.Business.Services.Interfaces;
using Company.Data.Repos.Interfaces;
using Company.RabbitMQ.Publisher;
using EmployeeDto = Company.Business.DTOs.Employee;
using EmployeeEntity = Company.Data.Entities.Employee;

namespace Company.Business.Services;

public class EmployeeService : Service<EmployeeEntity, EmployeeDto>, IEmployeeService
{
    protected new readonly IEmployeeRepository Repo;

    public EmployeeService(IEmployeeRepository repo, IMapper<EmployeeEntity, EmployeeDto> mapper, IPublisher<EmployeeEntity> publisher)
        : base(repo, mapper, publisher)
    {
        Repo = repo;
    }

    public async Task<ICollection<EmployeeDto>> GetAllFromCompany(int companyId, CancellationToken cancellationToken = default)
    {
        var entities = await Repo.GetAllFromCompany(companyId, cancellationToken);

        return Mapper.Map(entities);
    }
}