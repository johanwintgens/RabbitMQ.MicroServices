using Company.Business.Mappers;
using Company.Business.Mappers.Base;
using Company.Business.Services;
using Company.Business.Services.Base;
using Company.Business.Services.Interfaces;
using Company.Data.Extensions;
using Company.RabbitMQ.Publisher;
using Microsoft.Extensions.DependencyInjection;
using CompanyDto = Company.Business.DTOs.Company;
using CompanyEntity = Company.Data.Entities.Company;
using EmployeeDto = Company.Business.DTOs.Employee;
using EmployeeEntity = Company.Data.Entities.Employee;

namespace Company.Business.Extensions;

public static class ServiceCollectionExtensions
{
    public static void ConfigureBusinessDI(this IServiceCollection services)
    {
        // Business Layer
        services.AddScoped<ICompanyService, CompanyService>();
        services.AddScoped<IEmployeeService, EmployeeService>();

        services.AddScoped<IService<CompanyDto>, CompanyService>();
        services.AddScoped<IService<EmployeeDto>, EmployeeService>();

        services.AddScoped(typeof(IPublisher<>), typeof(Publisher<>));

        services.AddScoped<IMapper<CompanyEntity, CompanyDto>, CompanyMapper>();
        services.AddScoped<IMapper<EmployeeEntity, EmployeeDto>, EmployeeMapper>();

        services.ConfigureDataDI();
    }
}