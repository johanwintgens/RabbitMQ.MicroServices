﻿using Company.Business.DTOs.Base;

namespace Company.Business.DTOs;

public record Company(int Id, string Name) : Dto(Id);