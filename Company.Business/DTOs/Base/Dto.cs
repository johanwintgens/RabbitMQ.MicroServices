﻿namespace Company.Business.DTOs.Base;

public abstract record Dto(int Id) : IDto
{
    public int Id { get; set; } = Id;
}