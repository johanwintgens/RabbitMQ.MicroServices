﻿using Company.Business.DTOs.Base;

namespace Company.Business.DTOs;

public record Employee(int Id, string Name, string Email, int CompanyId) : Dto(Id);