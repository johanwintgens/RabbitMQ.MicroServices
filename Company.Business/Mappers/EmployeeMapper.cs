﻿using System.Collections.Generic;
using System.Linq;
using Company.Business.Mappers.Base;
using EmployeeDto = Company.Business.DTOs.Employee;
using EmployeeEntity = Company.Data.Entities.Employee;

namespace Company.Business.Mappers;

class EmployeeMapper : IMapper<EmployeeEntity, EmployeeDto>
{
    public EmployeeDto Map(EmployeeEntity source)
    {
        return new EmployeeDto
        (
            source.Id,
            source.Name,
            source.Email,
            source.CompanyId
        );
    }

    public EmployeeEntity Map(EmployeeDto source)
    {
        return new EmployeeEntity
        (
            source.Id,
            source.Name,
            source.Email,
            source.CompanyId
        );
    }

    public ICollection<EmployeeDto> Map(IEnumerable<EmployeeEntity> sources) => sources.Select(Map).ToList();
    public ICollection<EmployeeEntity> Map(IEnumerable<EmployeeDto> sources) => sources.Select(Map).ToList();
}