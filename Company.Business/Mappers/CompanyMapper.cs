﻿using System.Collections.Generic;
using System.Linq;
using Company.Business.Mappers.Base;
using CompanyDto = Company.Business.DTOs.Company;
using CompanyEntity = Company.Data.Entities.Company;

namespace Company.Business.Mappers;

class CompanyMapper : IMapper<CompanyEntity, CompanyDto>
{
    public CompanyDto Map(CompanyEntity source)
    {
        return new CompanyDto(source.Id, source.Name);
    }

    public CompanyEntity Map(CompanyDto source)
    {
        return new CompanyEntity
        (
           source.Id,
           source.Name
        );
    }

    public ICollection<CompanyDto> Map(IEnumerable<CompanyEntity> sources) => sources.Select(Map).ToList();
    public ICollection<CompanyEntity> Map(IEnumerable<CompanyDto> sources) => sources.Select(Map).ToList();
}